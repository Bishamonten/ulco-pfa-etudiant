data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche deriving Show

estWeekend :: Jour -> Bool
estWeekend Samedi = True
estWeekend Dimanche = True
estWeekend _ = False

-- compterOuvrables :: [Jour] -> Int
compterOuvrables :: [Jour] -> Int
compterOuvrables = length . filter (not . estWeekend)

main :: IO ()
main = do
    print $ estWeekend Lundi
    print $ estWeekend Samedi
    print $ compterOuvrables [Samedi, Lundi, Mardi, Dimanche]


