
-- TODO User

data User = User
    { _firstname :: String
    , _lastname  :: String
    , _age       :: Int
    }

showUser :: User -> String
showUser u = _firstname u ++ " " ++ _lastname u ++ " " ++ show (_age u)
 

-- incAge :: User -> User
incAge :: User -> User
incAge user = user { _age = _age user + 1 }

main :: IO ()
main = do
    let user = User "Hugo" "Monade" 21
    print $ showUser user
    let user1 = incAge user
    print $ showUser user1

