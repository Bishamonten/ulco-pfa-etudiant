{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}


import qualified Data.Text.Lazy as T
import Data.Aeson
import GHC.Generics

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    , address      :: Address 
    } deriving (Generic, Show)

instance ToJSON Person

data Address = Address 
    { number :: Int 
    , road :: T.Text
    , zipcode :: Int 
    , city :: T.Text    
    } deriving (Generic, Show)

instance ToJSON Address


persons :: [Person]
persons =
    [ Person "John" "Doe" 1970 (Address 1 "test" 59000 "Lille")
    , Person "Haskell" "Curry" 1900 (Address 1 "test" 59000 "Lille")
    ]

main :: IO ()
main = encodeFile "out-aeson1.json" persons