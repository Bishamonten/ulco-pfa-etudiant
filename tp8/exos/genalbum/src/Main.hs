import Site 
import Lucid 

main :: IO ()
main = do 
    mSites <- decodeFileStrinct "data/genalbum.json"
    case mSites of
        Nothing -> putStrLn "loading failed"
        Just sites -> mapM_ print (sites :: [Site])
    renderToFile "index.html" viewAlbum
