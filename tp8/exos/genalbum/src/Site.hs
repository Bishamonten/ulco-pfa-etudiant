
module Site where

import Data.Text.Lazy.IO as T

data Site = Site
    { imgs :: [String]
    , url :: T.Text
    } deriving (Generic, Show)

instance ToJSON Site