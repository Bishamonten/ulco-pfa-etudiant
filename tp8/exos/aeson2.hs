{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.Lazy as T
import Data.Aeson

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show)

instance ToJSON Person where
    toJSON (Person f l b) =
        object [ "first" .= f
               , "last"  .= l
               , "birth" .= show b
               ]

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970
    , Person "Haskell" "Curry" 1900
    ]

main :: IO ()
main = encodeFile "out-aeson1.json" persons