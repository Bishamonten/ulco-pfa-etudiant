{-# LANGUAGE OverloadedStrings #-}

import Lucid 

main :: IO ()
main = renderToFile "lucid2.html" maPage

maPage :: Html ()
maPage = do
    doctype_
    head_ $ do
        meta_ [charset_ "utf-8"]
    html_ $ body_ $ do
        h1_ "hello"
        img_ [src_ "https://en.sekaiproperty.com/img/sekai-property-post-image/file/6764/31d01b9e-9396-4657-8ad4-3206238afe52.jpg"]
        p_ $ do
            "this is"
            a_ [href_ "toto.png"] " a link"